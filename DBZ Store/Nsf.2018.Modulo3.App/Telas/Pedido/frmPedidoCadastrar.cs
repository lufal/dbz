﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;
using Nsf._2018.Modulo3.App.DB.Pedido;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmPedidoCadastrar : UserControl
    {
        BindingList<produtoDTO> carrinho = new BindingList<produtoDTO>();

        public frmPedidoCadastrar()
        {
            InitializeComponent();
            CarregarCombo();
            Grid();
        }

        private void btnEmitir_Click(object sender, EventArgs e)
        {
            PedidoDTO dto = new PedidoDTO();
            dto.Cliente = txtCliente.Text;
            dto.Cpf = txtCpf.Text;
            dto.Data = DateTime.Now;

            PedidoBusiness business = new PedidoBusiness();
            business.Salvar(dto, carrinho.ToList());

            MessageBox.Show("Pedido salvo com sucesso", "DBZ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();
        }

        void CarregarCombo()
        {
            produtoBusiness business = new produtoBusiness();
            List<produtoDTO> lista = new List<produtoDTO>();

            cboProduto.ValueMember = nameof(produtoDTO.Id);
            cboProduto.DisplayMember = nameof(produtoDTO.Nome);
            cboProduto.DataSource = lista;
        }

        void Grid()
        {
            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = carrinho;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            produtoDTO dto = cboProduto.SelectedItem as produtoDTO;

            for (int i = 0; i < int.Parse(txtQuantidade.Text); i++)
            {
                carrinho.Add(dto);
            }
        }
    }
}
