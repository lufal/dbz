﻿using Nsf._2018.Modulo3.App.DB.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoBusiness
    {
        public int Salvar(PedidoDTO pedido, List<produtoDTO> produto)
        {
            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            int idpedido = pedidoDatabase.Salvar(pedido);

            PedidoItemBusiness business = new PedidoItemBusiness();
            foreach (produtoDTO item in produto)
            {
                PedidoItemDTO dto = new PedidoItemDTO();
                dto.IdPedido = idpedido;
                dto.IdProduto = item.Id;

                business.Salvar(dto);
            }
            return idpedido;
        }

        public List<PedidoConsultarView> Consultar(string cliente)
        {
            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            return pedidoDatabase.Consultar(cliente);
        }
    }
}
