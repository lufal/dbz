﻿using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class produtoBusiness
    {
        produtoDatabase db = new produtoDatabase();

        public int Salvar(produtoDTO produto)
        {
            if (produto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }
            if (produto.Valor == 0)
            {
                throw new ArgumentException("Valor é obrigatório.");
            }
            return db.Salvar(produto);
        }

        public List<produtoDTO>Consultar(string Nome)
        {
            if (Nome == string.Empty)
            {
                Nome = string.Empty;
            }

            return db.Consultar(Nome);
        }
    }
}
