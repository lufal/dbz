﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
   class produtoDatabase
    {
        Database db = new Database();
        public int Salvar(produtoDTO dto)
        {
            string script =
                @"INSERT INTO tb_produto
                (
                nm_produto,
                vl_preco
                )
                VALUES
                (
                @nm_produto
                @vl_preco
                )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.Nome));
            parms.Add(new MySqlParameter("vl_preco", dto.Valor));

            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        public List<produtoDTO> Consultar(string Nome)
        {
            string script =
                @"SELECT * FROM tb_produto
                WHERE nm_produto like @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", "%" + Nome + "%"));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<produtoDTO> lista = new List<produtoDTO>();
            while (reader.Read())
            {
                produtoDTO novodto = new produtoDTO();
                novodto.Id = reader.GetInt32("id_produto");
                novodto.Nome = reader.GetString("nm_produto");
                novodto.Valor = reader.GetDecimal("vl_preco");
                lista.Add(novodto);
            }
            reader.Close();
            return lista;
        }
    }
}
